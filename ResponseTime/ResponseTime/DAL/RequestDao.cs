﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ResponseTime.DAL
{
    public class RequestDao
    {
        private const string ConnectionString = @"Data Source=FRY;Initial Catalog=RequestTime;Integrated Security=True";

        public static void AddReference(string reference, long latency)
        {
            using (SqlConnection connection =
                 new SqlConnection(ConnectionString))
            {
                DbCommand command = connection.CreateCommand();
                command.CommandText = "insert into dbo.Requests (Reference, Latency) values (@reference, @latency)";
                command.Parameters.Add(new SqlParameter("@reference", reference));
                command.Parameters.Add(new SqlParameter("@latency", latency));

                try
                {
                    connection.Open();
                    Console.WriteLine(command.ExecuteNonQuery());

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }

        }
    }
}