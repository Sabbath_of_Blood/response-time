﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using ResponseTime.DAL;

namespace ResponseTime.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        private readonly List<string> _hrefs = new List<string>();
        

        [HttpPost]

        public void PostUrl(string url)
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }

                string data = readStream.ReadToEnd();

                response.Close();
                readStream.Close();


                Match match;


                string HRefPattern = @"href\s*=\W(http(s)?\S+(?=\""))";

                try
                {
                    match = Regex.Match(data, HRefPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled,
                        TimeSpan.FromSeconds(1));

                    while (match.Success)
                    {

                        _hrefs.Add(match.Groups[1].ToString());
                        match = match.NextMatch();


                    }
                }
                catch (RegexMatchTimeoutException)
                {

                }
                Req();
            }
        }

        public void Req()
        {

            for (int i = 0; i < _hrefs.Count; i++)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(_hrefs[i]);
                HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                sw.Stop();
                long time = sw.ElapsedMilliseconds;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    response.Close();
                }
                RequestDao.AddReference(_hrefs[i], time);
            }
        }

    }
}



